import { types, Instance } from "mobx-state-tree";
import {ROOT_STORE, SESSION_STORE} from "@Shared/Constants";
import {Session} from "@Models/Session";

export const RootStore = types
    .model(ROOT_STORE, {
        [SESSION_STORE]: types.optional(Session, {}),
    })
    .views((self) => ({}))
    .actions((self) => ({}));

export interface IRootStore extends Instance<typeof RootStore> {}
