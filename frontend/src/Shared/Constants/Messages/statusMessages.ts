export const statusMessages = {
    COMPLETE: "Complete",
    ERROR: "Error",
    PENDING: "Pending",
}