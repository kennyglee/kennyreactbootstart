export const API_BASE = "/api";
export const SOME_API = API_BASE + "/someapi/";
export const SOME_API_WITH_PARAM =
    SOME_API + "?name=";

