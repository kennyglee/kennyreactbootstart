import { types, Instance } from "mobx-state-tree";

export const Session = types
    .model({
        name: types.maybe(types.string),
        welcomeMessage: "Session Welcome Message",
        is_done: types.optional(types.boolean, false),
        currentSomething: types.optional(types.string, ""),
    })
    .volatile((self) => ({}))
    .views((self) => ({
        status() {
            return self.is_done ? "Done" : "Not Done";
        },
    }))
    .actions((self) => ({
        markDone() {
            self.is_done = true;
        },
        updateWelcomeMessage(message: string) {
            self.welcomeMessage = message;
        },
    }))
    .actions((self) => ({
        setCurrentSomething(something: string) {
            self.currentSomething = something;
        },
    }));

export type ISession = Instance<typeof Session>;
