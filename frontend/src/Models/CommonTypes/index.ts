import { types } from "mobx-state-tree";

export const OptionalString = types.optional(types.string, "");
